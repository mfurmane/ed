import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dao.AuthorDAO;
import dao.BookDAO;
import dao.LibraryConnector;
import dto.BookDTO;

public class FileReader {

	private LibraryConnector libraryConnector = new LibraryConnector();
	private AuthorDAO authorDAO;
	private BookDAO bookDAO;
	private String authorName;

	// private String readFile(String path, Charset encoding) throws IOException
	// {
	// byte[] encoded = Files.readAllBytes(Paths.get(path));
	// return new String(encoded, encoding);
	// }

	public FileReader() {
		authorDAO = new AuthorDAO(libraryConnector);
		bookDAO = new BookDAO(libraryConnector);
	}

	private void readBook(File file) {
		if (file.isDirectory()) {
			boolean readed = false;

			for (File f : file.listFiles()) {
				String namePath;// = f.getAbsolutePath();
				if (!readed && !f.isDirectory()) {
					try {
						namePath = f.getAbsolutePath();
						java.io.FileInputStream fileInputStream = new java.io.FileInputStream(f);
						InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF8");
						BufferedReader authorer = new BufferedReader(inputStreamReader);
						authorName = authorer.readLine();
						fileInputStream.close();
						inputStreamReader.close();
						authorer.close();
						readed = true;
						File ff = new File(namePath);
						readBook(ff);

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					readBook(f);
				}
			}
		} else {
			int id = authorDAO.addAuthor(authorName);
			String title = "";
			try {
				java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
				InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF8");
				BufferedReader authorer = new BufferedReader(inputStreamReader);
				authorer.readLine();
				authorer.readLine();
				title+=authorer.readLine();
				authorer.readLine();
				String tom = authorer.readLine();
				if (tom.startsWith("Tom"))
				title+=(" - "+tom);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println(title);
			bookDAO.addBook(id, title, file);
		}
	}

	public void writeBooksToDatabase(String directory) {
		File root = new File(directory);
		readBook(root);
		for (BookDTO book : bookDAO.getBooksByAuthor(20)) {
			String[] lines = book.getContent().split("\\W");
			List<String> list = new ArrayList<String>(Arrays.asList(lines));
			list.removeAll(Arrays.asList("", null));
			Object[] ines = list.toArray();
			lines = new String[ines.length];
			int o = 0;
			for (Object object : ines) {
				lines[o++] = (String) object;
			}
		}
	}

	public AuthorDAO getAuthorDAO() {
		return authorDAO;
	}

	public BookDAO getBookDAO() {
		return bookDAO;
	}

}
