package linguistic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import dto.BookDTO;
import edu.uci.ics.jung.algorithms.scoring.BarycenterScorer;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;

public class OldLinguist {

	private static void addNext(Map<String, Occurrence> map, String key, String next) {
		if (!map.containsKey(key)) {
			map.put(key, new Occurrence(key));
			map.get(key).next.put(next, 1);
		} else {
			Occurrence o = map.get(key);
			if (o.next.containsKey(next))
				o.next.put(next, o.next.get(next) + 1);
			else
				o.next.put(next, 1);
		}
	}

	private static void addPrev(Map<String, Occurrence> map, String key, String previous) {
		if (!map.containsKey(key)) {
			map.put(key, new Occurrence(key));
			map.get(key).previous.put(previous, 1);
		} else {
			Occurrence o = map.get(key);
			if (o.previous.containsKey(previous))
				o.previous.put(previous, o.previous.get(previous) + 1);
			else
				o.previous.put(previous, 1);
		}
	}

	public static Map<String, Occurrence> findNeighbours(List<String> words, Map<String, Occurrence> map) {
		addNext(map, words.get(0), words.get(1));
		for (int i = 1; i < words.size() - 1; i++) {
			addPrev(map, words.get(i), words.get(i - 1));
			addNext(map, words.get(i), words.get(i + 1));
		}
		addPrev(map, words.get(words.size() - 1), words.get(words.size() - 2));
		return map;
	}

	public static void compareNeighbourCount(List<Map<String, Occurrence>> a) {
		List<String> words = new ArrayList<>();
		for (Map<String, Occurrence> author : a) {
			for (String word : author.keySet()) {
				words.add(word);
			}
		}
		Map<String, List<String>> wordNeighbours = new HashMap<>();
		for (String word : words) {
			for (Map<String, Occurrence> author : a) {
				// int c = 0;
				if (author.containsKey(word)) {
					wordNeighbours.put(word, new ArrayList<>());
					for (String i : author.get(word).previous.keySet()) {
						if (!wordNeighbours.get(word).contains(i))
							wordNeighbours.get(word).add(i);
					}
					for (String i : author.get(word).next.keySet()) {
						if (!wordNeighbours.get(word).contains(i))
							wordNeighbours.get(word).add(i);
					}
				}
			}
		}
		for (String string : wordNeighbours.keySet()) {
			System.out.println(string + ": " + wordNeighbours.get(string));
		}

	}

	public static Map<String, Map<Integer, Integer>> compareWordCount(List<Map<String, Occurrence>> a) {
		Map<String, Map<Integer, Integer>> wc = new HashMap<>();
		List<String> words = new ArrayList<>();

		for (Map<String, Occurrence> author : a) {
			for (String word : author.keySet()) {
				words.add(word);
			}
		}
		for (String word : words) {
			wc.put(word, new HashMap<>());
			int index = 0;
			for (Map<String, Occurrence> author : a) {
				if (author.containsKey(word)) {
					int c = 0;
					for (Integer i : author.get(word).previous.values())
						c += i;
					for (Integer i : author.get(word).next.values())
						c += i;
					wc.get(word).put(index, c / 2);
				} else
					wc.get(word).put(index, 0);
				index++;
			}
		}
		return wc;
	}

	public static Map<String, Map<Integer, Integer>> compareDiwordsCount(List<Map<String, Occurrence>> a) {
		Map<String, Map<Integer, Integer>> wc = new HashMap<>();
		Set<String> words = new HashSet<>();

		for (Map<String, Occurrence> author : a) {
			for (String word : author.keySet()) {
				for (String string : author.get(word).next.keySet()) {
					words.add(word + "->" + string);
				}
			}
		}
		for (String word : words) {
			wc.put(word, new HashMap<>());
			int index = 0;
			for (Map<String, Occurrence> author : a) {
				if (author.containsKey(word.split("->")[0])
						&& author.get(word.split("->")[0]).next.containsKey(word.split("->")[1])) {
					int c = author.get(word.split("->")[0]).next.get(word.split("->")[1]);
					wc.get(word).put(index, c);
				} else
					wc.get(word).put(index, 0);
				index++;
			}
		}
		return wc;
	}

}
