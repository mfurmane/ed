package linguistic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import dto.BookDTO;
import edu.uci.ics.jung.algorithms.scoring.BarycenterScorer;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import morfologik.stemming.WordData;
import morfologik.stemming.polish.PolishStemmer;

public class Linguist {
	private static PolishStemmer stemmer = new PolishStemmer();

	private static double compareChapters(Map<String, Double> previous, Map<String, Double> next) {
		for (String w : previous.keySet()) {
			if (!next.containsKey(w))
				next.put(w, 0.0);
		}
		for (String w : next.keySet()) {
			if (!previous.containsKey(w))
				previous.put(w, 0.0);
		}
		double similarity = 0;
		double EAiBi = 0;
		double EA2 = 0;
		double EB2 = 0;
		for (String w : previous.keySet()) {
			EAiBi += (previous.get(w) * next.get(w));
			EA2 += (Math.pow(previous.get(w), 2));
			EB2 += (Math.pow(next.get(w), 2));
		}
		similarity = EAiBi / (Math.sqrt(EA2) * Math.sqrt(EB2));
		return similarity;
	}

	public static void examineChapters(List<Graph<String, Integer>> graphs, List<Map<String, Double>> scores) {
		List<Double> similarities = new ArrayList<>();
		List<List<Double>> multisimilarities = new ArrayList<>();
		for (Map<String, Double> chapter : scores) {
			int index = scores.indexOf(chapter);
			if (index < scores.size() - 1) {
				similarities.add(compareChapters(chapter, scores.get(index + 1)));
			}
		}
		for (Map<String, Double> chapter : scores) {
			multisimilarities.add(new ArrayList<>());
			int index = scores.indexOf(chapter);
			for (Map<String, Double> chapter2 : scores) {
				if (!chapter.equals(chapter2)) multisimilarities.get(index).add(compareChapters(chapter, chapter2));
				else multisimilarities.get(index).add(-1.0);
			}
		}
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File("output/chapters.txt")), "utf-8"))) {
			for (Double double1 : similarities) {
				writer.write(double1+"\n");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File("output/multichapters.txt")), "utf-8"))) {
			for (List<Double> list : multisimilarities) {
				for (Double double2 : list) {
					if (double2 != -1) writer.write(double2+"");
					else writer.write("#");
					if (list.indexOf(double2)<list.size()-1) writer.write(";");
				}
				writer.write("\n");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void examineAction(List<Graph<String, Integer>> graphs, List<Map<String, Double>> scores,
			boolean adverbInDescription) {
		System.out.println("###EXAMINE ACTION");
		String fileName = "";
		if (adverbInDescription)
			fileName = "adverbInDescription.txt";
		else
			fileName = "fuckAdverb.txt";
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File("output/" + fileName)), "utf-8"))) {
			for (Graph<String, Integer> graph : graphs) {
				Map<String, Double> score = scores.get(graphs.indexOf(graph));
				Collection<String> col = graph.getVertices();
				double actionStrength = 0;
				double descriptionStrength = 0;
				for (String string : col) {
					if (score.containsKey(string)) {
						List<WordData> wd = stemmer.lookup(string);
						if (wd.size() > 0) {
							String tag = wd.get(0).getTag().toString().split(":")[0];
							if (tag.equals("subst"))
								descriptionStrength += score.get(string);
							else if (tag.equals("adj"))
								descriptionStrength += score.get(string);
							else if (tag.equals("adv") && adverbInDescription)
								descriptionStrength += score.get(string);
							else if (tag.equals("verb"))
								actionStrength += score.get(string);
						} else
							descriptionStrength += score.get(string);
					}
				}
				double proportion = actionStrength / (actionStrength + descriptionStrength);
				writer.write(graphs.indexOf(graph) + ":" + proportion + "\n");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void heroesExaminer(List<Graph<String, Integer>> graphs, List<Map<String, Double>> scores) {
		System.out.println("###EXAMINE HEROES");
		List<String> heroes = HeroService.getHeroes();
		Map<String, SortedSet<Integer>> map = new HashMap<>();
		for (String string : heroes) {
			map.put(string, new TreeSet<>());
		}
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File("output/heroes.txt")), "utf-8"))) {
			for (String hero : heroes) {
				for (Graph<String, Integer> graph : graphs) {
					if (graph.containsVertex(hero))
						map = HeroService.addChapterToHero(hero, graphs.indexOf(graph), map);
				}
				SortedMap<Integer, Double> mapForHero = new TreeMap<>();
				if (map.get(hero).size() > 0) {
					for (Integer in : map.get(hero)) {
						double x = 0;
						if (scores.get(in).get(hero) != null)
							x = scores.get(in).get(hero);
						double y = 0;
						if (scores.get(in).get(HeroService.getName(hero)) != null)
							y = scores.get(in).get(HeroService.getName(hero));
						mapForHero.put(in, x + y);
					}
					Writer writer2 = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(new File("output/heroes/" + hero + ".txt")), "utf-8"));
					for (Integer in : mapForHero.keySet()) {
						writer2.write(in + ":" + mapForHero.get(in) + "\n");
					}
					writer2.close();
					writer.write(hero + ":");
					String slicer = "";
					for (Integer i : map.get(hero)) {
						writer.write(slicer + i);
						slicer = ";";
					}
					writer.write("\n");
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Map<String, Set<String>> getContext(List<String> words, int bufor) {
		Map<String, Set<String>> context = new HashMap<>();
		for (int w = 0; w < words.size(); w++) {
			String word = words.get(w);
			if (!context.containsKey(word))
				context.put(word, new HashSet<>());
			for (int i = Math.max(0, w - bufor); i < Math.min(words.size() - 1, w + bufor); i++)
				if (i != w)
					context.get(word).add(words.get(i));
		}
		return context;
	}

	public static BookDTO prepareFightAndSword(BookDTO one, BookDTO two) {
		BookDTO dto = new BookDTO();
		dto.setBookId(one.getBookId() + 1);
		dto.setTitle("Ogniem i Mieczem");
		dto.setContent(one.getContent() + " " + two.getContent());
		return dto;
	}

	public static void examineBook(String auth, BookDTO bookDTO) {
		System.out.println("#EXAMINE BOOK " + auth + " - " + bookDTO.getTitle());
		List<Graph<String, Integer>> graphs = new ArrayList<>();
		for (List<String> li : BookParser.parseBook(bookDTO.getContent())) {
			Graph<String, Integer> partGraph = new SparseMultigraph<>();
			Map<String, Set<String>> context = Linguist.getContext(li, 5);
			int edgeId = 0;
			for (String wo : context.keySet()) {
				for (String string : context.get(wo)) {
					partGraph.addEdge(edgeId++, wo, string);
				}
			}
			graphs.add(partGraph);
		}
		compareGraphs(auth, bookDTO.getTitle(), graphs);
	}

	public static void compareGraphs(String author, String title, List<Graph<String, Integer>> graphs) {
		System.out.println("##COMPARE GRAPHS");
		List<Map<String, Double>> scores = saveGraphs(author, title, graphs, false);
		// heroesExaminer(graphs, scores);
		// examineAction(graphs, scores, true);
		// examineAction(graphs, scores, false);
		examineChapters(graphs, scores);
	}

	private static List<Map<String, Double>> saveGraphs(String author, String title,
			List<Graph<String, Integer>> graphs, boolean saveOnDisc) {
		List<Map<String, Double>> scoresMaps = new ArrayList<>();
		for (int i = 0; i < graphs.size(); i++) {
			scoresMaps.add(new HashMap<>());
			Graph<String, Integer> graph = graphs.get(i);
			BarycenterScorer<String, Integer> scorer = new BarycenterScorer<>(graph);
			Map<Double, String> scores = new HashMap<>();

			for (String string : graph.getVertices()) {
				scoresMaps.get(i).put(string, scorer.getVertexScore(string));
			}
			if (saveOnDisc) {
				if (!new File("output/" + author).exists())
					new File("output/" + author).mkdir();
				if (!new File("output/" + author + "/" + title).exists())
					new File("output/" + author + "/" + title).mkdir();
				try (Writer writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream("output/" + author + "/" + title + "/Rozdzia� " + i + ".txt"), "utf-8"))) {
					for (String key : scoresMaps.get(i).keySet()) {
						writer.write(scoresMaps.get(i).get(key) + ": " + key + "\n");
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return scoresMaps;
	}
}
