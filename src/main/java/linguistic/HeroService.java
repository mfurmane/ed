package linguistic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

public class HeroService {

	private static List<String> heroesToCheck = new ArrayList<>();
	private static List<String> heroes = new ArrayList<>();

	public static List<String> checkIfHero(String name, List<String> list) {
		if (heroesToCheck.size() == 0)
			prepareHeroes();
		if (heroesToCheck.contains(name.toLowerCase())) {
			if (name.equals("Tuhaj"))
				list.add("tuhaj-bej");
			else
				list.add(name.toLowerCase());
			return list;
		} else return list;
	}
	
	public static List<String> getHeroes() {
		if (heroes.size() == 0) prepareHeroes();
		return heroes;
	}
	
	public static String getName(String surname) {
		switch (surname) {
		case "skrzetuski":
			return "jan";
		case "chmielnicki":
			return "bohdan";
		case "podbipi�ta":
			return "longinus";
		case "wo�odyjowski":
			return "micha�";
		case "wi�niowiecki":
			return "jeremi";

		default:
			return"";
		}
	}
	
	public static Map<String, SortedSet<Integer>> addChapterToHero(String name, int part, Map<String, SortedSet<Integer>> map) {
		if (name.equals("jan"))
			map.get("skrzetuski").add(part);
		else if (name.equals("bohdan"))
			map.get("chmielnicki").add(part);
		else if (name.equals("longinus"))
			map.get("podbipi�ta").add(part);
		else if (name.equals("micha�"))
			map.get("wo�odyjowski").add(part);
		else if (name.equals("jeremi"))
			map.get("wi�niowiecki").add(part);
		else
			map.get(name).add(part);
		return map;
	}

	public static boolean prepareHeroes() {
		heroesToCheck.add("zag�oba");
		heroesToCheck.add("longinus");
		heroesToCheck.add("tuhaj");
		heroesToCheck.add("bohdan");
		heroesToCheck.add("krzywonos");
		heroesToCheck.add("za�wilichowski");
		heroesToCheck.add("horpyna");

		heroes.add("zag�oba");
		heroes.add("longinus");
		heroes.add("podbipi�ta");
		heroes.add("micha�");
		heroes.add("wo�odyjowski");
		heroes.add("jan");
		heroes.add("skrzetuski");
		heroes.add("tuhaj-bej");
		heroes.add("helena");
		heroes.add("bohun");
		heroes.add("jeremi");
		heroes.add("wi�niowiecki");
		heroes.add("bohdan");
		heroes.add("chmielnicki");
		heroes.add("za�wilichowski");
		heroes.add("horpyna");
		heroes.add("rz�dzian");
		
		return true;
	}

}
