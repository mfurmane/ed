package linguistic;

import java.util.HashMap;
import java.util.Map;

public class Occurrence {
	public String key;
	public Map<String, Integer> previous = new HashMap<>();
	public Map<String, Integer> next = new HashMap<>();

	public Occurrence(String key) {
		super();
		this.key = key;
	}

}
