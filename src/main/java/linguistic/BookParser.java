package linguistic;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import morfologik.stemming.WordData;
import morfologik.stemming.polish.PolishStemmer;

public class BookParser {

	private static PolishStemmer stemmer = new PolishStemmer();
	private static boolean logStats = false;
	private static List<String> stoplist = Stoplist.prepare();
	private static Map<String, Integer> unfound = new HashMap<>();
	private static Set<String> uppercase = new HashSet<>();
	private static int xx = 0;

	public static List<String> parter(String book) { // it means it slice text
														// to parts
		List<String> parts = new ArrayList<>();
		String[] p = new String[1];
		int prologue = 0;
		if (Pattern.compile("\nWst�p").matcher(book).find()) {
			prologue = 1;
			book = book.split("\nWst�p")[1];
		}

		if (Pattern.compile("Rozdzia�[ \\w\\p{L}]+").matcher(book).find())
			p = book.split(Pattern.compile("Rozdzia�[ \\w\\p{L}]+").pattern());
		else if (Pattern.compile("ROZDZIA�[ \\w\\p{L}]+").matcher(book).find())
			p = book.split(Pattern.compile("ROZDZIA�[ \\w\\p{L}]+").pattern());
		else if (Pattern.compile("\\n\\d+\\.[ \\w\\p{L}]+").matcher(book).find())
			p = book.split(Pattern.compile("\\n\\d+\\.[ \\w\\p{L}]+").pattern());
		else if (Pattern.compile("\\n\\w+\\.[ [IVXLC]\\p{L}]+").matcher(book).find())
			p = book.split(Pattern.compile("\\n[IVXLC]+\\.[ \\w\\p{L}]+").pattern());
		else if (Pattern.compile("\\n[IVXLC]+" + System.getProperty("line.separator")).matcher(book).find())
			p = book.split(Pattern.compile("\\n[IVXLC]+" + System.getProperty("line.separator")).pattern());
		// else System.out.println("HUI");
		for (int i = 1 - prologue; i < p.length; i++) {
			// if (i==0) System.out.println(p[0]);
			parts.add(p[i]);
		}
		return parts;
	}

	public static Map<String, Integer> getWordCount(List<String> list) {
		Map<String, Integer> map = new HashMap<>();
		for (String string : list) {
			if (map.containsKey(string))
				map.put(string, map.get(string) + 1);
			else
				map.put(string, 1);
		}
		return map;
	}

	private static void saveUppercase() {
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream("output/uppercase" + xx + ".txt"), "utf-8"))) {
			for (String upp : uppercase) {
				writer.write(upp + "\n");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void addToUnfound(String u) {
		if (unfound.containsKey(u))
			unfound.put(u, unfound.get(u) + 1);
		else
			unfound.put(u, 1);
	}

	private static void saveUnfound() {
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream("output/unfound" + xx + ".txt"), "utf-8"))) {
			for (String string : unfound.keySet()) {
				writer.write(string + ": " + unfound.get(string) + "\n");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		xx++;
	}

	public static List<List<String>> parseBook(String book) {
		uppercase.clear();
		unfound.clear();
		double good = 0;
		double bad = 0;
		List<List<String>> doneParts = new ArrayList<>();
		List<String> parts = parter(book);
		for (String part : parts) {
			String[] lines = part.split("[^\\w\\p{L}]+");
			List<String> list = new ArrayList<String>(Arrays.asList(lines));
			list.removeAll(Arrays.asList("", null));
			List<String> trueList = new ArrayList<>();
			for (String string : list) {
				List<WordData> wd = stemmer.lookup(string.toLowerCase());
				if (wd.isEmpty()) {
					wd = stemmer.lookup(string);
					if (wd.isEmpty()) {
						int lsize = trueList.size();
						trueList = HeroService.checkIfHero(string, trueList);
						if (lsize == trueList.size()) {
							addToUnfound(string);
							bad++;
						} else {
							uppercase.add(string);
							good++;
						}
					} else {
						uppercase.add(wd.get(0).getStem().toString());
						good++;
						trueList.add(wd.get(0).getStem().toString().toLowerCase());
					}
				} else {
					good++;
					trueList.add(wd.get(0).getStem().toString());
				}
			}
			trueList.removeAll(stoplist);
			List<String> hapaxLegomena = new ArrayList<>();
			Map<String, Integer> map = getWordCount(trueList);
			for (String string : map.keySet()) {
				if (map.get(string) < 3)
					hapaxLegomena.add(string);
			}
			trueList.removeAll(hapaxLegomena);
			doneParts.add(trueList);
			if (logStats)
				System.out.println(
						"Good: " + (good / (good + bad) * 100) + "%; Bad: " + (bad / (good + bad) * 100) + "%");
		}
		saveUppercase();
		saveUnfound();
		return doneParts;
	}

	public static void setLogStats(boolean lStats) {
		logStats = lStats;
	}

}
