package dao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import dto.BookDTO;

public class BookDAO {

	public LibraryConnector connector;
	private Statement statement;

	public BookDAO(LibraryConnector conn) {
		connector = conn;
	}

	public List<BookDTO> getBooksByAuthor(int authorId) {
		Vector<BookDTO> books = new Vector<BookDTO>();
		try {
			statement = connector.connect();
			ResultSet results = statement.executeQuery("SELECT * FROM book WHERE authorId='"+authorId+"'");

			while (results.next()) {
				BookDTO dto = new BookDTO();
				dto.setBookId(results.getInt("bookId"));
				dto.setContent(results.getString("content"));
				dto.setTitle(results.getString("title"));
				books.add(dto);
			}

			connector.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return books;
	}

	public void addBook(int id, String title, File content) {
		try {
			statement = connector.connect();
			String sql = "INSERT INTO book (authorId, content, title) VALUES (?, ?, ?)";
			PreparedStatement prep = connector.getPreparedStatement(sql);
			prep.setInt(1, id);
			prep.setAsciiStream(2, new FileInputStream(content), (int) content.length());
			prep.setString(3, title);
			prep.executeUpdate();
			connector.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addBook(int authorId, String title, String content) {
		File file = new File("tmp.txt");
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(file), "utf-8"))) {
			writer.write(content);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		addBook(authorId, title, file);
	}

}
