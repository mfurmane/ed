package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import dto.AuthorDTO;

public class AuthorDAO {

	private LibraryConnector connector;
	private Statement statement;
	private BookDAO bookDAO;

	public AuthorDAO(LibraryConnector conn) {
		connector = conn;
		bookDAO = new BookDAO(conn);
	}

	public List<AuthorDTO> getAuthors() {
		Vector<AuthorDTO> authors = new Vector<AuthorDTO>();
		try {
			statement = connector.connect();
			ResultSet results = statement.executeQuery("SELECT * FROM author");

			while (results.next()) {
				AuthorDTO dto = new AuthorDTO();
				dto.setAuthorId(results.getInt("authorId"));
				dto.setName(results.getString("name"));
				dto.setBooks(bookDAO.getBooksByAuthor(dto.getAuthorId()));
				authors.add(dto);
			}

			connector.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return authors;
	}

	public int addAuthor(String name) {
		int id = 0;
		try {
			statement = connector.connect();
			ResultSet results = statement.executeQuery("SELECT * FROM author");
			boolean notExists = true;
			while (results.next()) {
				id = results.getInt("authorId");
				if (results.getString("name").equals(name)) {
					notExists = false;
					break;
				}
			}
			connector.closeConnection();
			if (notExists) {
				statement = connector.connect();
				String sql = "INSERT INTO author (name) VALUES ('"+name+"')";
				statement.executeUpdate(sql);
				connector.closeConnection();
				statement = connector.connect();
				ResultSet result2 = statement.executeQuery("SELECT * FROM author");
				while (result2.next()) {
					id = result2.getInt("authorId");
					if (result2.getString("name").equals(name)) {
						notExists = false;
						break;
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}

}
