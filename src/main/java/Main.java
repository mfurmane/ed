import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dao.BookDAO;
import dao.LibraryConnector;
import dto.AuthorDTO;
import dto.BookDTO;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import linguistic.BookParser;
import linguistic.Linguist;
import linguistic.Occurrence;
import morfologik.stemming.polish.PolishStemmer;

public class Main {
	public static void presentForTwoAuthors(Map<String, Map<Integer, Integer>> ass, int single, boolean fuckZeros) {
		for (String s : ass.keySet()) {
			String inter = "";
			int i = single;
			while (s.toString().length() < (7 + i * 8))
				i--;
			for (int a = 0; a < (single - i); a++)
				inter += "\t";
			if (!fuckZeros || (fuckZeros && ass.get(s).get(0) > 0 && ass.get(s).get(1) > 0))
				System.out.println(s + ":" + inter + ass.get(s).get(0) + " - " + ass.get(s).get(1));
		}
	}

	public static void main(String[] args) {
		FileReader f = new FileReader();
//		 f.writeBooksToDatabase("data");
		List<AuthorDTO> authors = f.getAuthorDAO().getAuthors();
		List<BookDTO> books = f.getBookDAO().getBooksByAuthor(authors.get(authors.size() - 1).getAuthorId());
		// for (BookDTO bookDTO : books) {
		// List<String> b = BookParser.parseBook(bookDTO.getContent());
		// }
		// List<String> b = BookParser.parseBook(books.get(0).getContent());
		// int i = 0;
		// for (String string : b) {
		// i++;
		// if (i < 10)
		// System.out.print(string+"\t");
		// else {
		// System.out.println(string);
		// i = 0;
		// }
		// }
		// Map<String, List<String>> map = new HashMap<>();
		// Linguist.findNeighbours(BookParser.parseBook(books.get(0).getContent()),
		// map);

//		int authorIndex = 0;
		List<Map<String, Occurrence>> library = new ArrayList<Map<String, Occurrence>>();
//		System.out.print("\t\t");
//		String a = ": ";
		for (AuthorDTO auth : authors) {
//			System.out.print(a + auth.getName());
			library.add(new HashMap<>());
			List<BookDTO> boo = f.getBookDAO().getBooksByAuthor(auth.getAuthorId());
//			BookDTO bookDTO = Linguist.prepareFightAndSword(boo.get(1), boo.get(0));
			Linguist.examineBook(auth.getName(), boo.get(2));
//			authorIndex++;
		}
		System.out.println();

//		Map<String, Map<Integer, Integer>> ass = Linguist.compareWordCount(library);
//		presentForTwoAuthors(ass, 3, false);

		// Map<String, Map<Integer, Integer>> as =
		// Linguist.compareDiwordsCount(library);
		// presentForTwoAuthors(as, 5, true);

		// for (Map<String, Occurrence> map2 : library) {
		// for (String string : map2.keySet()) {
		//// System.out.println(string + " : " + map2.get(string).key+" :
		// "+map2.get(string).previous+" : "+map2.get(string).next);
		// }
		// }

		// for (String string : map.keySet()) {
		// System.out.println(string+" : "+map.get(string));
		// // }
	}

}
